python-sptest (0.2.1-4) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:07:21 -0500

python-sptest (0.2.1-3) unstable; urgency=medium

  * Team upload.

  [ Stephan Peijnik ]
  * debian/rules
    - Add --destdir=* --download-version=$(DEB_UPSTREAM_VERSION) parameters
      to uscan call in get-orig-source target.
    - Define DEB_UPSTREAM_VERSION variable.

  [ Jakub Wilk ]
  * Build-depend on python.  Closes: #796647
  * Fix a typo in the package description.

  [ SVN-Git Migration ]
  * Update Vcs fields for git migration.

  [ Mattia Rizzolo ]
  * Use source format 3.0 (quilt).
  * Use debhelper compat 9.
  * Bump Standards-Versions to 3.9.6, no changes needed.
  * Depend on ${misc:Depends}, since we use debhelper.
  * debian/rules: convert to short dh format and build using pybuild.
  * debian/watch: rewrite using pypi.debian.net redirector.
  * Run wrap-and-sort.

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 20 Jan 2016 18:38:55 +0000

python-sptest (0.2.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build using dh-python. Closes: #786123.

 -- Matthias Klose <doko@debian.org>  Sat, 22 Aug 2015 10:10:54 +0200

python-sptest (0.2.1-2) unstable; urgency=medium

  * Removed "build install" dependency of binary-arch rule in debian/rules.
  * Fixed wrong Architecture (should be "all", not "any") in debian/control.
    + build using default Python version only (fixes FTBFS bug,
      Closes: #503466)
  * Fixed debian/copyright to get rid of a lintian warning.
  * Removed duplicate binary-arch target from debian/rules.
  * Added Debian Python Modules Team to Uploaders.
  * Changed required debhelper version to "5" in debian/control and
    + debian/compat.
  * Added Vcs-Browser and Vcs-Svn fields to debian/control.
  * Removed Provides field from debian/control (not needed by Python modules).
  * Switched from pycentral to pysupport.

 -- Stephan Peijnik <debian@sp.or.at>  Tue, 28 Oct 2008 23:02:39 +0100

python-sptest (0.2.1-1) unstable; urgency=low

  * Initial release (Closes: #498586)

 -- Stephan Peijnik <debian@sp.or.at>  Thu, 11 Sep 2008 12:46:16 +0200
