sptest
======

sptest is an extension to Python's unittest module (and framework). It provides
an easier way of customizing how unit test results are handled.
sptest is Free Software and licensed under the GPL, version 3 or later.
See the COPYING file you received with sptest for the license text.

It comes with two pre-built output modules, namely FancyCLIOutput and 
XMLOutput.

Whilst FancyCLIOutput provides coloured command line output, including
a test summary, XMLOutput creates an XML file containing the results of
the tests that have been run.

Existing Python unittest testcases and testsuites do not have to be modified
in order to work with sptest. sptest.TestMain accepts a single Python unittest
TestSuite instance as argument and processes the tests in the TestSuite.

Customization can be done by implementing the sptest.output.IOutput interface
and passing it to TestMain.

sptest also comes with full doxygen-enabled source documentation and a doxygen
configuration file (doxygen.cfg) in the doc/ directory.
Also, doc/examples/ contains two simple sptest usage examples.

At http://www.bitbucket.org/sp/sptest bug reports can be filed and the
development can be tracked.